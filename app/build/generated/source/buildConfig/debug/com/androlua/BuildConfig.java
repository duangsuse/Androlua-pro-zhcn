/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.androlua;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.androlua";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 325;
  public static final String VERSION_NAME = "3.2.5";
}
